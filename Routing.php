<?php

require_once 'Controllers/SecurityController.php';
require_once 'Controllers/RegisterController.php';
require_once 'Controllers/MainController.php';
require_once 'Controllers/SearchController.php';
require_once 'Controllers/AddController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'main' => [
                'controller' => 'MainController',
                'action' => 'Clicked'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'register' =>[
                'controller' =>'RegisterController',
                'action' => 'register'
            ],
            'search' =>[
                'controller' =>'SearchController',
                'action' => 'search'
            ],
            'add' =>[
                'controller' =>'AddController',
                'action' => 'add'
            ],
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'login';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}