<?php

class User {
    private $id;
    private $login;
    private $password;
   // private $name;
//    private $surname; // TODO user details.
//    private $role = ['ROLE_USER']; // TODO ROLE

    public function __construct(
        string $login,
        string $password,
        //string $name,
     //   string $surname,
        int $id = null
    ) {
        $this->login = $login;
        $this->password = $password;
     //   $this->name = $name;
     //   $this->surname = $surname;
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getLogin(): string
    {
        return $this->login;
    }


   // public function getRole(): array
   // {
        //return $this->role;
  //  } // TODO ROLE

  //  public function getName(): string  TODO unlock name and surname in future
    //{
     //   return $this->name;
    //}

  //  public function getSurname(): string
   // {
    //    return $this->surname;
   // }
}