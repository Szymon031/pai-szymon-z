<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="styleseet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="styleseet" type="text/css" href="../../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet" />
    <?php include(dirname(__DIR__) . '/Common/head.php');?>
    <style>
        <?php include(dirname(__DIR__).'../../Public/css/style.css');?>
    </style>
    <title>add</title>
</head>
<body>
<div class="container" >
    <form action="?page=add" method="POST">
        <h1 >Add Task</h1>
        <input type="text" placeholder="name" name="name" id="name" required>
        <input type="text" placeholder="description" name="description" id="description" required>
        <input type="datetime-local" placeholder="date" name="date" id="date" required>
        <input type="text" placeholder="tag;tag;tag" name="tags" id="tags" required>
        <button type="submit" class="registerbtn">Add Task</button>
</div>
</body>
</html>