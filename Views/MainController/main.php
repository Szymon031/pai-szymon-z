<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="styleseet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="styleseet" type="text/css" href="../../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet" />
    <?php include(dirname(__DIR__) . '/Common/head.php');?>
    <?php
    require_once(dirname(__DIR__) . '../../Repository/TaskRepository.php');
    require_once(dirname(__DIR__) . '../../Models/Task.php');
    $TaskRepository = new TaskRepository();
    $task =  $TaskRepository->getCurrentTask();
    $tasks = $TaskRepository->getAllTasksAscByDate();
    ?>
    <style>
        <?php include(dirname(__DIR__).'../../Public/css/style.css');?>
    </style>
    <title>Activities</title>
</head>
<body>
<div class="horizontalcontainer" >
    <form action="?page=add" method="POST">
        <button name="add" value="yes" type="submit">Add</button>
    </form>
    <form action="?page=search" method="POST">
<p style="text-align: center;"> <?php echo( date('d-m-Y H:i:s'));?></p>
   <h1>Next activity:</h1>
<p style="text-align: left;">Name:&nbsp;<?php echo($task->getName());?></p>
<p style="text-align: left;">Tags:&nbsp;</p>
<p style="text-align: left;">Description: <?php echo($task->getDescription());?></p>
<p style="text-align: left;">Date:&nbsp;<?php echo($task->getDate());?></p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">Today:
    <?php
    $boolechoed = 0;
    $tomorrow = (new DateTime('tomorrow'))->format('Y-m-d H:i:s');
    $tomorrowdatestamp = strtotime( $tomorrow);
    foreach ($tasks as $row)
    {
        $taskdatestamp = strtotime($row['taskdate']);

    if($taskdatestamp <= $tomorrowdatestamp)
    {
        if($boolechoed)
        {
            echo(' , ');
        }
        echo($row['name']);     // TODO echo tags/date
        $boolechoed = 1;
    }
    }
    ?>
</p>
<p style="text-align: left;">This week:
        <?php
        $boolechoed = 0;
        $nextweek = (new DateTime())->add(new DateInterval('P1W'))->format('Y-m-d');
        $nextweekdatestamp = strtotime( $nextweek);
        foreach ($tasks as $row)
        {
            $taskdatestamp = strtotime($row['taskdate']);

            if($taskdatestamp > $tomorrowdatestamp and $taskdatestamp <= $nextweekdatestamp)
            {
                if($boolechoed)
                {
                    echo(' , ');
                }
                echo($row['name']);     // TODO echo tags/date
                $boolechoed = 1;
            }
        }
        ?>
</p>
<p style="text-align: left;">This month:
        <?php
        $boolechoed = 0;
        $nextmonth = (new DateTime())->modify( 'next month' )->format('Y-m-d');
        $nextmonthdatestamp = strtotime( $nextmonth);
        foreach ($tasks as $row)
        {
            $taskdatestamp = strtotime($row['taskdate']);

            if($taskdatestamp > $nextweekdatestamp and $taskdatestamp <= $nextmonthdatestamp)
            {
                if($boolechoed)
                {
                    echo(' , ');
                }
                echo($row['name']);     // TODO echo tags/date
                $boolechoed = 1;
            }
        }
        ?>
</p>
<p style="text-align: left;">&nbsp;</p>
    </form>
    <form action="?page=search" method="POST">
        <button name="search" value="yes" type="submit">Search&nbsp;</button>
    </form>
</div>
</body>
</html>