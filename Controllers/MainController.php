<?php

require_once 'AppController.php';
//require_once __DIR__.'//..//Models//User.php';
//require_once __DIR__.'//..//Repository//UserRepository.php';

class MainController extends AppController {

    public function Clicked()
    {
        $add = isset($_POST['add']) ? $_POST['add'] : '';
        if($add === "yes")
            $this->render('add');

        $search = isset($_POST['search']) ? $_POST['search'] : '';
        if($search === "yes")
            $this->render('search');

        $this->render('main');
    }
}