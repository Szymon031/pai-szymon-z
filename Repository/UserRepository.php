<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/User.php';

class UserRepository extends Repository
{

    public function getUser(string $login): ? User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT login, userpassword, id FROM appuser WHERE login = :login
        ');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['login'],
            $user['userpassword'],
         //   $user['name'],
          //  $user['surname'],
            $user['id']
        );
    }

    public function getUsers(): array {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM appuser WHERE login != :login;
        ');
        $stmt->bindParam(':login', $_SESSION['id'], PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function SetUser(string $login, string $password)
    {
        $stmt = $this->database->connect()->prepare('INSERT INTO appuser (login, userpassword, enabled, fkappuserdetailsid) VALUES ( :login, :password, 1, 1);');
        $stmt->bindParam(':login', $login, PDO::PARAM_STR); // TODO details.
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->execute();
    }
}