<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="styleseet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet" />
    <?php include(dirname(__DIR__).'/Common/head.php');?>
    <style>
        <?php include(dirname(__DIR__).'../../Public/css/style.css');?>
    </style>
    <title>login</title>
</head>
<body>
<div class="container" >
        <div class="logo">
            <?php include(dirname(__DIR__).'../../Public/images/logo.svg');?>
        </div>
    <form action="?page=login" method="POST" >
        <div class="messages">
            <?php
            if(isset($messages)){
                foreach($messages as $message) {
                    echo $message;
                }
            }
            ?>
        </div>
        <input name="login" type="text" placeholder="login" class="button" >
        <input name="password" type="password" placeholder="password" class="button" >
        <button type="submit">CONTINUE</button>
    </form>
    <form action="?page=register" method ="POST" >
        <button name="register" value="yes" type="submit">register</button>
    </form>
</div>
</body>
</html>