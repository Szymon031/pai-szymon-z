<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="styleseet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="styleseet" type="text/css" href="../../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet" />
    <?php include(dirname(__DIR__).'/Common/head.php');?>
    <style>
        <?php include(dirname(__DIR__).'../../Public/css/style.css');?>
    </style>
    <title>search</title>
</head>
<body>

<div class="horizontalcontainer" >
    <form action="?page=search" method="POST" >
        <button name="back" value="yes" type="BACK">back</button>
    </form>
    <form action="?page=search" method="POST" >
        <h1>Search by name or tag</h1>
        <input name="search" placeholder="name #tag ; #tag" type="text" class="button" >
        <button type="search">SEARCH</button>
        <div class="messages">
        <?php
        if(isset($searcheddescriptions)){
            foreach($searcheddescriptions as $founddescription)
            {
                    echo '<br /> Description: '.$founddescription;
            }
        }
        if(isset($searchednames)) {
            foreach ($searchednames as $foundname) {
                echo '<br /> Name: ' . $foundname;
            }
        }
        if(isset($searcheddate)){
            foreach($searcheddate as $founddate)
            {
                echo '<br /> Date: '.$founddate;
            }
        }
        ?>
        </div>
    </form>
    <form action="?page=search" method="POST" >
    </form>

</div>
</body>
</html>