<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Task.php';
require_once __DIR__.'//..//Repository//TaskRepository.php';

class AddController extends AppController {

    public function add()
    {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : '';
        $tags = isset($_POST['tags']) ? $_POST['tags'] : '';

        if($name and $date and $description and $tags and $_SESSION['userid'])
        {
            $TaskRepository = new TaskRepository();
            $TaskRepository->SetTask($name, $date, $_SESSION['userid'] , $description);
            $url = "http://$_SERVER[HTTP_HOST]/PAI/";
            header("Location: {$url}?page=main");
            $this->render('main'); // TODO give an info after add?
            return;                             // TODO dodawanie tagow tez zrobic
        }
        $this->render('add');
    }
}