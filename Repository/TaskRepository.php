<?php

require_once "Repository.php";
require_once __DIR__.'/../Models/Task.php';

class TaskRepository extends Repository
{
    public function getTask(string $name): ? Task
    {
        $stmt = $this->database->connect()->prepare('
            SELECT name, taskdate, description, id FROM tasks WHERE name = :name
        ');
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->execute();

        $task = $stmt->fetch(PDO::FETCH_ASSOC);

        if($task == false) {
            return null;
        }

        return new Task(
            $task['taskdate'],
            $task['description'],
            $task['name'],
            $task['id'],
        );
    }

    public function getCurrentTask(): ? Task
    {
        $stmt = $this->database->connect()->prepare('SELECT name, taskdate, description, id
            FROM tasks where tasks.taskdate >= NOW() order by tasks.taskdate limit 1;');
        $stmt->execute();

        $task = $stmt->fetch(PDO::FETCH_ASSOC);

        if($task == false) {
            return null;
        }

        return new Task(
            $task['taskdate'],
            $task['description'],
            $task['name'],
            $task['id'],
        );
    }
    public function getAllTasksAscByDate(): array
    {
        $stmt = $this->database->connect()->prepare('SELECT name, taskdate, description, id
            FROM tasks where tasks.taskdate >= NOW() order by tasks.taskdate;');
        $stmt->execute();
        /*$tasks = array();
        while(($row = $stmt->fetch(PDO::FETCH_ASSOC))) {
            $tasks[$row['id_customer']] = $row['name_customer'];
        }*/

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
/*
        if($tasks == false) {
            return null;
        }

        return new Task(
            $task['taskdate'],
            $task['description'],
            $task['name'],
            $task['id'],
        );*/
    }
    public function SetTask(string $namee, string $taskdate, int  $fkappuserid, string $description)
    {
        $stmt = $this->database->connect()->prepare('INSERT INTO tasks (name, taskdate, fkappuserid, description) VALUES ( :namee, :taskdate, :fkappuserid, :description);');
        $stmt->bindParam(':namee', $namee, PDO::PARAM_STR);
        $stmt->bindParam(':taskdate', $taskdate, PDO::PARAM_STR);
        $stmt->bindParam(':fkappuserid', $fkappuserid, PDO::PARAM_INT);
        $stmt->bindParam(':description', $description, PDO::PARAM_STR);
        $stmt->execute();
    }
}