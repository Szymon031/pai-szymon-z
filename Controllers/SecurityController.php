<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $user = $userRepository->getUser($login);
            $register = isset($_POST['register']) ? $_POST['register'] : '';
                if($register === "yes")
                    $this->render('register');

            if (!$user) {
                $this->render('login', ['messages' => ['User with this login not exist!']]);
                return;
            }

            if ($user->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }

            $_SESSION["id"] = $user->getLogin();
            $_SESSION["userid"] = $user->getId();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/PAI?page=main");
            $this->render('main');
            return;
        }

        $this->render('login');
    }
    public function register()
    {
        $this->render('register');
    }
    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['You have been successfully logged out!']]);
    }
}