<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <link rel="styleseet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="styleseet" type="text/css" href="../../Public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet" />
    <?php include(dirname(__DIR__) . '/Common/head.php');?>
    <style>
        <?php include(dirname(__DIR__).'../../Public/css/style.css');?>
    </style>
    <title>register</title>
</head>
<body>
<div class="container" >
<form action="?page=register" method="POST">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>
    <input type="text" placeholder="login" name="login" id="login" required>
    <input type="password" placeholder="Password" name="password" id="password" required>
    <input type="password" placeholder="Repeat Password" name="password-repeat" id="password-repeat" required>
    <hr>

    <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>
    <button type="submit" class="registerbtn">Register</button>
</form>
    <form action="?page=register" method="POST">
    <button type="submit" class="registerbtn" name="gologin" value="yes">Back to login</button>

    </form>

  </div>

</body>
</html>