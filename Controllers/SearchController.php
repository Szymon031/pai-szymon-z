<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//Task.php';
require_once __DIR__.'//..//Repository//TaskRepository.php';

class SearchController extends AppController
{
    public function search()
    {
        $back = isset($_POST['back']) ? $_POST['back'] : '';
        if($back)
        {
            if($back === 'yes')
            {
                $url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: {$url}/PAI?page=main");
                $this->render('main');
                return;
            }
        }
        $search = isset($_POST['search']) ? $_POST['search'] : '';
        if($search)
        {
            $TaskRepository = new TaskRepository();
            $searchedrecords = $TaskRepository->getTask($search);  //TODO tags, search multiple elements
            if($searchedrecords)
                $this->render('search', ['searcheddescriptions' => [$searchedrecords->getDescription()],
                                                'searchednames' => [$searchedrecords->getName()],
                                                'searcheddate' => [$searchedrecords->getDate()]]);
            else
                $this->render('search');

            return;
        }

        $this->render('search');
    }
}