<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class RegisterController extends AppController {
    public function register()
    {
        if ($this->isPost())
        {
            $gologin=  isset($_POST['gologin']) ? $_POST['gologin'] : '';
            if($gologin === 'yes')
            {
                $url = "http://$_SERVER[HTTP_HOST]/PAI/";
                header("Location: {$url}?page=login");
                $this->render('login');
                return;
            }
            $login = isset($_POST['login']) ? $_POST['login'] : '';
            $password = isset($_POST['password']) ? $_POST['password'] : '';
           if($login and $password)
            {
                $userRepository = new UserRepository();
                $userRepository->SetUser($login, $password);
                $url = "http://$_SERVER[HTTP_HOST]/PAI/";
                header("Location: {$url}?page=login");
                $this->render('login'); // TODO give an info after regi
                return;
         }
        }
        $this->render('register');

    }
}