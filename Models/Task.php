<?php

class Task {
    private $id;
    private $date;
    private $description;
   // private $tags; // TODO tags
    private $name;

    public function __construct(
        string $date,
        string $description,
        //array $tags,
        string $name,
        int $id = null
    ) {
        $this->date = $date;
        $this->description = $description;
      //  $this->tags = $tags;
        $this->id = $id;
        $this->name=$name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDate(): string
    {
        return $this->date;
    }

   /* public function getTags(): array
    {
        return $this->tags;
    }*/

    public function getDescription(): string
    {
        return $this->description;
    }
}